/*This class used for creating the access token */
public class AccessTokenGenerator{

  public static AccessTokenResponseWrapper.AccessTokenResponse getAccessTokenResponse(String customSettingName){
       Mule_Web_Service__c myGardAccessToken=Mule_Web_Service__c.getValues(customSettingName);
           if(myGardAccessToken.Enabled__c){
             try{
              HttpRequest request = new HttpRequest();
           request.setEndpoint(myGardAccessToken.Endpoint_Url__c);
            request.setMethod(Label.Post);
            request.setHeader(label.grantType, label.clientCredentials);            
            request.setHeader(Label.clientId, myGardAccessToken.Client_Id__c);            
            request.setHeader(Label.clientSecret, myGardAccessToken.Client_secret__c);
             request.setHeader(Label.scope, Label.ReadWrite);
            request.setTimeout(120000);
            return ((AccessTokenResponseWrapper.AccessTokenResponse) JSON.deserialize(new Http().send(request).getBody(),AccessTokenResponseWrapper.AccessTokenResponse.class));
            }catch(CalloutException ex){
              system.debug(ex.getMessage());
              Logger__c logMessage=new Logger__c(  Log_Description__c=ex.getMessage()+ ': ' + ex.getLineNumber());
              insert logMessage;
              return null;
            }
          }
          else{
             return null;
          }
      }
}